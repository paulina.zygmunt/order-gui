# Order GUI
# Creates an order based on the items selected by the user

from tkinter import *

class Application(Frame):
    def __init__(self, master):
        super(Application, self).__init__(master)  
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        Label(self,
              text = "FILL IN THE INFORMATION FOR A NEW ORDER."
              ).grid(row = 0, column = 0, columnspan = 2, sticky = W)

        Label(self,
              text = "YOUR NAME:"
              ).grid(row = 1, column = 0, sticky = W)
        self.person_ent = Entry(self)
        self.person_ent.grid(row = 1, column = 1, sticky = W)

        Label(self,
              text = "ADDRESS:"
              ).grid(row = 2, column = 0, sticky = W)
        self.address_ent = Entry(self)
        self.address_ent.grid(row = 2, column = 1, sticky = W)

        Label(self,
              text = "MOBILE PHONE:"
              ).grid(row = 3, column = 0, sticky = W)
        self.mobile_ent = Entry(self)
        self.mobile_ent.grid(row = 3, column = 1, sticky = W)


        Label(self,
              text = "SMALL"
              ).grid(row = 4, column = 1, sticky = W)
        Label(self,
              text = "MEDIUM"
              ).grid(row = 4, column = 2, sticky = W)
        Label(self,
              text = "LARGE"
              ).grid(row = 4, column = 3, sticky = W)

        Label(self,
              text = "MARGHERITTA"
              ).grid(row = 5, column = 0, sticky = W)
        self.margheritta = StringVar()
        self.margheritta.set(None)
        margherittas = ["27.00", "37.00", "47.00"]
        column = 1
        for margheritta in margherittas:
            Radiobutton(self,
                        text = margheritta,
                        variable = self.margheritta,
                        value = margheritta
                        ).grid(row = 5, column = column, sticky = W)
            column += 1

        Label(self,
              text = "PEPPERONI"
              ).grid(row = 6, column = 0, sticky = W)
        self.pepperoni = StringVar()
        self.pepperoni.set(None)
        pepperonis = ["30.00", "40.00", "50.00"]
        column = 1
        for pepperoni in pepperonis:
            Radiobutton(self,
                        text = pepperoni,
                        variable = self.pepperoni,
                        value = pepperoni
                        ).grid(row = 6, column = column, sticky = W)
            column += 1

        Label(self,
              text = "CAPRICCIOSA"
              ).grid(row = 7, column = 0, sticky = W)
        self.capricciosa = StringVar()
        self.capricciosa.set(None)
        capricciosas = ["28.00", "38.00", "48.00"]
        column = 1
        for capricciosa in capricciosas:
            Radiobutton(self,
                        text = capricciosa,
                        variable = self.capricciosa,
                        value = capricciosa
                        ).grid(row = 7, column = column, sticky = W)
            column += 1

        Label(self,
              text = "FARMER"
              ).grid(row = 8, column = 0, sticky = W)
        self.farmer = StringVar()
        self.farmer.set(None)
        farmers = ["32.00", "42.00", "52.00"]
        column = 1
        for farmer in farmers:
            Radiobutton(self,
                        text = farmer,
                        variable = self.farmer,
                        value = farmer
                        ).grid(row = 8, column = column, sticky = W)
            column += 1

        Label(self,
              text = "VEGETARIANA"
              ).grid(row = 9, column = 0, sticky = W)
        self.vegetariana = StringVar()
        self.vegetariana.set(None)
        vegetarianas = ["28.00", "38.00", "48.00"]
        column = 1
        for vegetariana in vegetarianas:
            Radiobutton(self,
                        text = vegetariana,
                        variable = self.vegetariana,
                        value = vegetariana
                        ).grid(row = 9, column = column, sticky = W)
            column += 1


        Label(self,
              text = "ADDITIONAL SAUCE:"
              ).grid(row = 10, column = 0, sticky = W)

        def select_first_radiobutton():
            self.sauce_type.set(sauce_types[0])

        self.is_sauce = BooleanVar()
        Checkbutton(self,
                    text = "6.00",
                    variable = self.is_sauce,
                    command=select_first_radiobutton
                    ).grid(row = 10, column = 1, sticky = W)

        self.sauce_type = StringVar()
        self.sauce_type.set(None)
        sauce_types = ["TOMATO", "GARLIC", "SPICY"]
        column = 1
        for sauce_type in sauce_types:
            Radiobutton(self,
                        text = sauce_type,
                        variable = self.sauce_type,
                        value = sauce_type
                        ).grid(row = 11, column = column, sticky = W)
            column += 1


        Button(self,
               text = "CLICK TO SUMMARISE YOUR ORDER",
               command = self.write_summary
               ).grid(row = 12, column = 0, sticky = W)

        self.summary_txt = Text(self, width = 75, height = 10, wrap = WORD)
        self.summary_txt.grid(row = 13, column = 0, columnspan = 4)


    def write_summary(self):
        person = self.person_ent.get()
        address = self.address_ent.get()
        mobile = self.mobile_ent.get()
        price = 0
        order = ""
        if self.margheritta.get() != 'None':
            order += "Margheritta, "
            price += float(self.margheritta.get())
        if self.pepperoni.get() != 'None':
            order += "Pepperoni, "
            price += float(self.pepperoni.get())
        if self.capricciosa.get() != 'None':
            order += "Capricciosa, "
            price += float(self.capricciosa.get())
        if self.farmer.get() != 'None':
            order += "Farmer, "
            price += float(self.farmer.get())
        if self.vegetariana.get() != 'None':
            order += "Vegetariana, "
            price += float(self.vegetariana.get())

        sauce = "no additional sauce."
        if self.is_sauce.get():
            sauce = self.sauce_type.get().lower() + " sauce."
            price += 6.00

        # create the summarise
        summary = "Delivery details: \n"
        summary += person + ", " + address + ", " + mobile
        summary += ". \n"
        summary += "Your order: \n"
        summary += "Pizza "
        summary += order
        summary += sauce
        summary += "\nPrice: "
        summary += str(price) + " PLN."

        self.summary_txt.delete(0.0, END)
        self.summary_txt.insert(0.0, summary)

root = Tk()
root.title("Order")
app = Application(root)
root.mainloop()
